import { createMuiTheme } from '@material-ui/core'

export const colorsDark = {
    primary: {
        light: '#484848',
        main: '#212121',
        dark: '#000000',
    },
    secondary: {
        light: '#60ad5e',
        main: '#2E7D32',
        dark: '#005005',
    },
}

export const colorsLight = {}

export const themeDark = createMuiTheme({
    palette: {
        primary: {
            main: colorsDark.primary.main,
        },
        secondary: {
            main: colorsDark.secondary.main,
        },
    },
})

// export const themeLight = createMuiTheme({
//   palette: {
//     primary: {
//       main: colorsLight.primary.main,
//     },
//     secondary: {
//       main: colorsLight.secondary.main,
//     },
//   },
// });
