import React from 'react'
import Main from './screens/Main/Main'

export const routes = [
    {
        path: '/',
        exact: true,
        main: <Main />,
    },
]
