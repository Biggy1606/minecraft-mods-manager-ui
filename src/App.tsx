import * as React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { routes } from './routes'

const App = (): JSX.Element => {
    return (
        <BrowserRouter>
            <Switch>
                {routes.map((element, index) => (
                    <Route
                        key={index}
                        path={element.path}
                        exact={element.exact}
                    >
                        {element.main}
                    </Route>
                ))}
            </Switch>
        </BrowserRouter>
    )
}

export default App
