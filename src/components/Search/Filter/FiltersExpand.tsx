import React from 'react'
import { Collapse } from '@material-ui/core'

const FiltersExpand = (props: FiltersExpandProps): JSX.Element => {
    return (
        <Collapse in={props.expanded} timeout="auto" unmountOnExit>
            <>FiltersExpand component</>
        </Collapse>
    )
}

export default FiltersExpand
