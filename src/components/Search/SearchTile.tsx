import React from 'react'
import {
    Button,
    Card,
    CardActions,
    FormControl,
    Grid,
    GridSize,
    IconButton,
    MenuItem,
    TextField,
} from '@material-ui/core'
import { displaypage, sortby } from './searchData'
import { useStyles } from './searchStyles'
import FiltersExpand from './Filter/FiltersExpand'
import { ExpandMore } from '@material-ui/icons'
import clsx from 'clsx'

export interface Size {
    xs?: GridSize
    sm?: GridSize
    md?: GridSize
    lg?: GridSize
    xl?: GridSize
}

interface Sizes {
    search: Size
    version: Size
    sort: Size
    page: Size
}

const sizes: Sizes = {
    search: {
        xs: 12,
        sm: 6,
        md: 3,
    },
    version: {
        xs: 12,
        sm: 6,
        md: 3,
    },
    sort: {
        xs: 12,
        sm: 6,
        md: 3,
    },
    page: {
        xs: 12,
        sm: 6,
        md: 3,
    },
}

type TextFieldType = 'search' | 'sort' | 'display' | 'version' | 'debug'

interface SearchTileProps {
    styles?: string | undefined
}

const SearchTile: React.FC<SearchTileProps> = (props: SearchTileProps) => {
    const classes = useStyles()
    const [search, setSearch] = React.useState('Default value')
    const [sort, setSort] = React.useState('popular')
    const [display, setDisplay] = React.useState('20')
    const [version, setVersion] = React.useState('1.12.2')
    const [filtersExpanded, setFiltersExpanded] = React.useState(false)

    const handleChange = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
        type: TextFieldType
    ): void => {
        switch (type) {
            case 'search':
                setSearch(event.target.value)
                break
            case 'sort':
                setSort(event.target.value)
                break
            case 'display':
                setDisplay(event.target.value)
                break
            case 'version':
                setVersion(event.target.value)
                break
            case 'debug':
                console.log(event)
                break
        }
        console.log(search)
    }

    const handleExpandClick = (): void => {
        setFiltersExpanded(!filtersExpanded)
    }

    const handleSearchInput = (
        e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
    ): void => {
        setSearch(e.target.value)
    }

    const handleSearchFieldClick = (): void => {
        if (search === 'Default value') setSearch('')
    }

    return (
        <Card className={props.styles}>
            <Grid container justify="center" className={classes.grid}>
                <Grid
                    item
                    xs={sizes.search.xs}
                    sm={sizes.search.sm}
                    md={sizes.search.md}
                >
                    <FormControl className={classes.filter}>
                        <TextField
                            label={'Search'}
                            variant={'outlined'}
                            onChange={(event) => handleSearchInput(event)}
                            value={search}
                            onClick={() => handleSearchFieldClick()}
                        />
                    </FormControl>
                </Grid>
                <Grid
                    item
                    xs={sizes.version.xs}
                    sm={sizes.version.sm}
                    md={sizes.version.md}
                >
                    <FormControl className={classes.filter}>
                        <TextField
                            label={'Minecraft version'}
                            variant={'outlined'}
                            select
                            value={version}
                            onChange={(event) => handleChange(event, 'version')}
                        >
                            <MenuItem key={0} value={'1.12.2'}>
                                1.12.2
                            </MenuItem>
                        </TextField>
                    </FormControl>
                </Grid>
                <Grid
                    item
                    xs={sizes.sort.xs}
                    sm={sizes.sort.sm}
                    md={sizes.sort.md}
                >
                    <FormControl className={classes.filter}>
                        <TextField
                            label={'Sort by'}
                            variant={'outlined'}
                            select
                            value={sort}
                            onChange={(event) => handleChange(event, 'sort')}
                        >
                            {sortby.map((element) => (
                                <MenuItem
                                    key={element.key}
                                    value={element.value}
                                >
                                    {element.label}
                                </MenuItem>
                            ))}
                        </TextField>
                    </FormControl>
                </Grid>
                <Grid
                    item
                    xs={sizes.page.xs}
                    sm={sizes.page.sm}
                    md={sizes.page.md}
                >
                    <FormControl className={classes.filter}>
                        <TextField
                            label={'Display per page'}
                            variant={'outlined'}
                            select
                            value={display}
                            onChange={(event) => handleChange(event, 'display')}
                        >
                            {displaypage.map((value, index) => (
                                <MenuItem key={index} value={value}>
                                    {value}
                                </MenuItem>
                            ))}
                        </TextField>
                    </FormControl>
                </Grid>
            </Grid>
            <FiltersExpand expanded={filtersExpanded} />
            <CardActions disableSpacing>
                <Button type={'submit'} variant={'contained'} color={'primary'}>
                    Search
                </Button>
                <IconButton
                    className={clsx(classes.expand, {
                        [classes.expandOpen]: filtersExpanded,
                    })}
                    onClick={handleExpandClick}
                    aria-expanded={filtersExpanded}
                    aria-label="show more filters"
                >
                    <ExpandMore />
                </IconButton>
            </CardActions>
        </Card>
    )
}

export default SearchTile
