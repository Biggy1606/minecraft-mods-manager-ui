export const sortby = [
    { label: 'Popularity', value: 'popular', key: 0 },
    { label: 'Name', value: 'name', key: 1 },
    { label: 'Date created', value: 'creation', key: 2 },
    { label: 'Last updated', value: 'update', key: 3 },
    { label: 'Total downloads', value: 'downloads', key: 4 },
]
export const displaypage = [20, 50, 100, 500, 1000]

export const versions = ['1.12.2', '1.16.5']
