import React, { useState } from 'react'
import {
    Card,
    CardContent,
    Collapse,
    Grid,
    IconButton,
    Typography,
} from '@material-ui/core'
import { ExpandMore } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'

const useStyles = makeStyles((theme) => ({
    name: {
        flexGrow: 1,
        paddingLeft: 16,
    },
    logo: {
        width: 64,
        height: 64,
    },
    arrow: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
        width: theme.spacing(8),
        height: theme.spacing(8),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    collapsible: {
        padding: theme.spacing(2),
    },
    cardContent: {
        padding: theme.spacing(1),
        display: 'flex',
    },
}))

const ModCard = (props: ModCardProps): JSX.Element => {
    const classes = useStyles()
    const [isOpen, setIsOpen] = useState(false)

    const handleOpen = (): void => {
        setIsOpen(!isOpen)
    }

    return (
        <Card>
            <CardContent className={classes.cardContent}>
                <img className={classes.logo} src={props.logo} />

                <Typography className={classes.name} variant={'h6'}>
                    {props.name} {props.author}
                </Typography>
                <IconButton
                    onClick={handleOpen}
                    className={clsx(classes.arrow, {
                        [classes.expandOpen]: isOpen,
                    })}
                >
                    <ExpandMore />
                </IconButton>
            </CardContent>

            <Collapse
                in={isOpen}
                timeout="auto"
                className={classes.collapsible}
            >
                <h4>Summary:</h4>
                <p>{props.summary}</p>
            </Collapse>
        </Card>
    )
}

export default ModCard
