interface ModCardProps {
    mod?: any
    name?: string
    logo?: string
    summary?: string
    author?: string
}
