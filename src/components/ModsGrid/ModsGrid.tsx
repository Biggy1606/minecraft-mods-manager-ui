import React, { ReactElement } from 'react'
import { Grid, makeStyles, Typography } from '@material-ui/core'
import ModCard from './ModCard/ModCard'
import image from '../../assets/BigosCraft.png'

const useStyles = makeStyles((theme) => ({
    modCard: {
        padding: theme.spacing(1),
    },
    emptyList: {
        color: 'white',
    },
}))

const ModsGrid = (props: IModsGridProps): JSX.Element => {
    const classes = useStyles()

    const generateList = (modsList: Array<any>): Array<ReactElement> => {
        if (modsList.length === 0) {
            return [
                <Typography
                    key={0}
                    className={classes.emptyList}
                    variant={'h6'}
                >
                    Empty list of mods
                </Typography>,
            ]
        } else {
            return modsList.map((value: any, index: number) => (
                <Grid key={index} item xs={10} className={classes.modCard}>
                    <ModCard
                        name={value.name}
                        logo={value.logo ? value.logo.thumbnailUrl : image}
                        summary={value.summary}
                        author={value.authors[0].name}
                    />
                </Grid>
            ))
        }
    }

    return (
        <Grid container justify={'center'}>
            {generateList(props.modsList)}
        </Grid>
    )
}

export default ModsGrid
