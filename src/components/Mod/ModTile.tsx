import React, { ReactElement } from 'react'
import {
    Button,
    Card,
    CardActionArea,
    CardActions,
    CardContent,
    CardMedia,
    Typography,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

interface IModTitleProps {
    styles?: string
    modName: string
}

const useStyles = makeStyles((theme) => ({
    media: {
        height: 14,
    },
}))

const ModTile = (props: IModTitleProps): JSX.Element => {
    const classes = useStyles()
    return (
        <Card style={{ margin: 10 }}>
            <CardActionArea>
                <CardMedia className={classes.media} title={props.modName} />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {props.modName}
                    </Typography>
                    <Typography variant={'subtitle2'}>by Author</Typography>
                    <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                    >
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit. Assumenda consequatur corporis illo. Ad adipisci
                        eos, error est explicabo labore magnam nemo officiis
                        praesentium quam sint totam. Earum minus modi sint.
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button size="small" color="secondary" variant="contained">
                    Download
                </Button>
                <Button size="small" color="primary" variant="outlined">
                    Add
                </Button>
            </CardActions>
        </Card>
    )
}

export default ModTile
