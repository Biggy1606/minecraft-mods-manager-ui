import React from 'react'
import { Button, Grid } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
    mainGridContainer: {
        color: 'white',
        padding: 10,
    },
    button: {
        marginRight: 0,
    },
}))

const ModpacksGrid: React.FC = () => {
    const classes = useStyles()
    return (
        <>
            <Grid
                container
                justify={'center'}
                className={classes.mainGridContainer}
            >
                <Grid item xs={4}>
                    <Button
                        variant={'outlined'}
                        color={'secondary'}
                        className={classes.button}
                    >
                        Download
                    </Button>
                </Grid>
                <Grid item xs={4}>
                    <Button
                        variant={'outlined'}
                        color={'secondary'}
                        className={classes.button}
                    >
                        Download
                    </Button>
                </Grid>
                <Grid item xs={4}>
                    <Button
                        variant={'outlined'}
                        color={'secondary'}
                        className={classes.button}
                    >
                        Download
                    </Button>
                </Grid>
            </Grid>
        </>
    )
}

export default ModpacksGrid
