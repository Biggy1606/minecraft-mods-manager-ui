import { Size } from '../../components/Search/SearchTile'

interface MainSizes {
    mainGrid: Size
    searchGrid: Size
}
