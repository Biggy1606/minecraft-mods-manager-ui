import React, { useEffect } from 'react'
import { CssBaseline, Grid, MuiThemeProvider } from '@material-ui/core'
import SearchTile from '../../components/Search/SearchTile'
import { themeDark } from '../../theme'
import { useStyles } from './mainStyles'
import ModsGrid from '../../components/ModsGrid/ModsGrid'
import { modsListStatic } from './dataMain'
import curseforge from 'mc-curseforge-api'

const Main = (): JSX.Element => {
    const classes = useStyles()

    useEffect(() => {
        curseforge
            .getMods()
            .then((mods) => {
                console.log(mods)
            })
            .catch((error) => {
                console.log(error)
            })
    })

    return (
        <>
            <MuiThemeProvider theme={themeDark}>
                <CssBaseline />
                <Grid
                    container
                    justify={'center'}
                    className={classes.mainGridContainer}
                >
                    <Grid item xs={6}>
                        <Grid container justify={'center'}>
                            <Grid item className={classes.searchGridContainer}>
                                <SearchTile />
                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid item xs={6} className={classes.modsGridContainer}>
                        <ModsGrid modsList={modsListStatic} />
                    </Grid>
                </Grid>
            </MuiThemeProvider>
        </>
    )
}

export default Main
