import { makeStyles } from '@material-ui/core'
import { colorsDark } from '../../theme'

export const useStyles = makeStyles((theme) => ({
    mainGridContainer: {
        background: colorsDark.primary.main,
    },
    searchGridContainer: {
        padding: theme.spacing(2),
        border: () => 'solid 10px' + colorsDark.secondary.light,
    },
    modsGridContainer: {
        overflowY: 'scroll',
        height: '100vh',
    },
}))
